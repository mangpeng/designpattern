﻿using System;
using System.Collections;
using UnityEngine;

public class FlyWeightFactory : IFlyWeight<EngKorData>
{
    //싱글톤
    private static FlyWeightFactory instance;
    private FlyWeightFactory()   {    }
    public static FlyWeightFactory Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new FlyWeightFactory();
            }
            return instance;
        }
    }

    Hashtable pool = new Hashtable();

    public EngKorData GetData(object _key, object _value)
    {
        EngKorData data = pool[_key] as EngKorData;

        if(data == null)
        {
            Debug.Log(String.Format($"{0} 데이터 생성", _key));
            data = new EngKorData(_key, _value);
            pool.Add(_key, data);
        }
        else
        {
            Debug.Log(String.Format($"{0} 데이터 재사용", _key));
        }

        return data;
    }

    public int GetSize()
    {
        return pool.Count;
    }
}
