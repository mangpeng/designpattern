﻿public interface IFlyWeight<T>
{
    int GetSize();
    T GetData(object key, object value);
}
