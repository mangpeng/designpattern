﻿public class EngKorData
{
    string eng = string.Empty;
    string kor = string.Empty;
    private object _key;
    private object _value;

    public EngKorData(string _eng, string _kor)
    {
        eng = _eng;
        kor = _kor;
    }

    public EngKorData(object key, object value)
    {
        _key = key;
        _value = value;
    }
}
